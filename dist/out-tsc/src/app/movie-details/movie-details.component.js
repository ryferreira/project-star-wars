import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { MovieService } from '../movies/movie.service';
var MovieDetailsComponent = /** @class */ (function () {
    function MovieDetailsComponent(activeRoute, movieService) {
        this.activeRoute = activeRoute;
        this.movieService = movieService;
        this.displayedColumns = ['episode_id', 'title', 'release_dates'];
        this.starshipsId = [];
    }
    MovieDetailsComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.episode_id = this.activeRoute.snapshot.params['id'];
        this.movieService.getMovie(this.episode_id)
            .subscribe(function (movie) {
            _this.movie = movie;
            _this.getStarsihpsId();
            // console.log(">>>>>>>>>>>>> " + this.movie.starships[0]);
        });
        // console.log("STAR: " + this.starshipsId);
        // console.log("Starship: " + this.movie.starships[0]);
    };
    MovieDetailsComponent.prototype.getStarsihpsId = function () {
        var link;
        var temp;
        var length;
        // let aux: string;
        for (var _i = 0, _a = this.movie.starships; _i < _a.length; _i++) {
            link = _a[_i];
            temp = link.split('/');
            length = temp.length;
            this.starshipsId.push(Number(temp[length - 2]));
        }
        this.movie.starshipsId = this.starshipsId;
        // console.log(this.starshipsId);
        // aux = this.movie.starships[0];
        // temp = aux.split('/');
        // length = temp.length;
        // temp = Number(this.movie.starships[0].split('/')[length-2]);
        // console.log("NUMBER: " + temp[length-2]);
        // debugger
    };
    MovieDetailsComponent = tslib_1.__decorate([
        Component({
            selector: 'app-movie-details',
            templateUrl: './movie-details.component.html',
            styleUrls: ['./movie-details.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [ActivatedRoute, MovieService])
    ], MovieDetailsComponent);
    return MovieDetailsComponent;
}());
export { MovieDetailsComponent };
//# sourceMappingURL=movie-details.component.js.map