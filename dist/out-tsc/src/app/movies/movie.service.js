import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { MOVIE_API } from './movie.api';
var MovieService = /** @class */ (function () {
    function MovieService(http) {
        this.http = http;
    }
    MovieService.prototype.extractData = function (res) {
        var body = res;
        return body || {};
    };
    MovieService.prototype.getMovies = function () {
        return this.http.get(MOVIE_API + "/films/").pipe(map(this.extractData));
    };
    MovieService.prototype.getMovie = function (id) {
        return this.http.get(MOVIE_API + "/films/" + id + "/");
        //   .pipe(map(this.extractData));
    };
    MovieService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root',
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], MovieService);
    return MovieService;
}());
export { MovieService };
//# sourceMappingURL=movie.service.js.map