import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
import { MovieService } from './movie.service';
var MoviesComponent = /** @class */ (function () {
    // movies: Movie[];
    function MoviesComponent(movieService) {
        this.movieService = movieService;
        this.displayedColumns = ['title', 'release_date'];
    }
    MoviesComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.movieService.getMovies()
            .subscribe(function (movies) {
            console.log(movies["results"]);
            _this.dataSource = movies['results'];
            // this.movie = this.dataSource[0] as Movie;
            // console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>> " + this.movie);
            // this.movies = movies['results'];
            // let movieAux: Movie;
            // movies = JSON.stringify(movies);
            // this.dataSource = movies["title"];
            // this.parseJson(movies as string);
            // console.log(">>>>>>>>>>>>>>> " + movies);
            // this.dataSource = [];
            // this.dataSource.push(movies);
        });
    };
    MoviesComponent = tslib_1.__decorate([
        Component({
            selector: 'app-movies',
            templateUrl: './movies.component.html',
            styleUrls: ['./movies.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [MovieService])
    ], MoviesComponent);
    return MoviesComponent;
}());
export { MoviesComponent };
var Movie = /** @class */ (function () {
    function Movie(title, episode_id, release_date, starships, starshipsId) {
        this.title = title;
        this.episode_id = episode_id;
        this.release_date = release_date;
        this.starships = starships;
        this.starshipsId = starshipsId;
    }
    return Movie;
}());
export { Movie };
//# sourceMappingURL=movies.component.js.map