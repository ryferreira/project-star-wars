import * as tslib_1 from "tslib";
import { Component } from '@angular/core';
var TitleComponent = /** @class */ (function () {
    function TitleComponent() {
        this.title = 'Star Wars';
    }
    TitleComponent.prototype.ngOnInit = function () {
    };
    TitleComponent = tslib_1.__decorate([
        Component({
            selector: 'app-title',
            templateUrl: './title.component.html',
            styleUrls: ['./title.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [])
    ], TitleComponent);
    return TitleComponent;
}());
export { TitleComponent };
//# sourceMappingURL=title.component.js.map