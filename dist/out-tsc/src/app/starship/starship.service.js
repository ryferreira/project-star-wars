import * as tslib_1 from "tslib";
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { MOVIE_API } from '../movies/movie.api';
var StarshipService = /** @class */ (function () {
    function StarshipService(http) {
        this.http = http;
    }
    StarshipService.prototype.getStarship = function (id) {
        return this.http.get(MOVIE_API + "/starships/" + id + "/");
    };
    StarshipService = tslib_1.__decorate([
        Injectable({
            providedIn: 'root'
        }),
        tslib_1.__metadata("design:paramtypes", [HttpClient])
    ], StarshipService);
    return StarshipService;
}());
export { StarshipService };
//# sourceMappingURL=starship.service.js.map