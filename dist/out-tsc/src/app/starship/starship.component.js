import * as tslib_1 from "tslib";
import { Component, Input, ChangeDetectorRef } from '@angular/core';
import { StarshipService } from './starship.service';
import { tap } from 'rxjs/operators';
var StarshipComponent = /** @class */ (function () {
    function StarshipComponent(starshipService, detector) {
        this.starshipService = starshipService;
        this.detector = detector;
        this.displayedColumns = ['name', 'model', 'starship_class'];
        this.starships = [];
        this.dataSource = [];
    }
    StarshipComponent.prototype.ngOnInit = function () {
        // console.log(">> " + this.starshipsId)
        // debugger
        for (var _i = 0, _a = this.starshipsId; _i < _a.length; _i++) {
            var id = _a[_i];
            console.log("ID: " + id);
            this.starshipService.getStarship(id)
                .pipe(tap(function (x) { return console.log(x); }));
            // subscribe((data) => this.teste(data))
            // .subscribe(data => {
            //   let starship: Starship;
            //     console.log(data);
            //     starship = data
            //     this.starships.push(starship);
            //     // this.dataSource.push(starship);
            //     // debugger
            //   })
        }
        console.log(this.starships);
    };
    StarshipComponent.prototype.teste = function (data) {
        this.detector.detectChanges();
        this.starships.push(data);
        this.dataSource = this.starships;
    };
    tslib_1.__decorate([
        Input(),
        tslib_1.__metadata("design:type", Array)
    ], StarshipComponent.prototype, "starshipsId", void 0);
    StarshipComponent = tslib_1.__decorate([
        Component({
            selector: 'app-starship',
            templateUrl: './starship.component.html',
            styleUrls: ['./starship.component.css']
        }),
        tslib_1.__metadata("design:paramtypes", [StarshipService, ChangeDetectorRef])
    ], StarshipComponent);
    return StarshipComponent;
}());
export { StarshipComponent };
var Starship = /** @class */ (function () {
    function Starship(name, model, starship_class) {
        this.name = name;
        this.model = model;
        this.starship_class = starship_class;
    }
    return Starship;
}());
export { Starship };
//# sourceMappingURL=starship.component.js.map