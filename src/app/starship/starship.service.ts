import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { MOVIE_API } from '../movies/movie.api';
import { Starship } from './starship.component';

@Injectable({
  providedIn: 'root'
})
export class StarshipService {

  constructor(private http: HttpClient) { }

  getStarship(id: number) {
    return this.http.get<Starship>(`${MOVIE_API}/starships/${id}/`);
  }
}
