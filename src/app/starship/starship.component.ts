import { Component, OnInit, Input, ChangeDetectorRef, ChangeDetectionStrategy } from '@angular/core';
import { StarshipService } from './starship.service';
import { tap, map } from 'rxjs/operators';

@Component({
  selector: 'app-starship',
  templateUrl: './starship.component.html',
  styleUrls: ['./starship.component.css'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class StarshipComponent implements OnInit {
  displayedColumns: string[] = ['name', 'model', 'starship_class'];

  @Input() starshipsId: number[];

  starships: Starship[] = [];
  dataSource: Starship[] = [];

  constructor(private starshipService: StarshipService, private detector: ChangeDetectorRef) {
    
  }

  ngOnInit() {
    for (let id of this.starshipsId) {
      this.starshipService.getStarship(id)
      .subscribe(data => {
          let starship: Starship;

          starship = data;
          this.addData(starship);
          this.starships.push(starship);
          this.detector.detectChanges();
        })
    }

    console.log(this.starships);
  }

  addData(data) {
    this.dataSource = this.dataSource.concat(data);
  }

}

export class Starship {
  constructor (
    public name: string,
    public model: string,
    public starship_class: string,
    // public manufacturer: string,
    // public cost_in_credits: string,
    // public length: string,
    // public crew: string,
    // public passengers: string,
    // public max_atmosphering_speed: string,
    // public hyperdrive_rating: string,
    // public MGLT: string,
    // public cargo_capacity: string,
    // public consumables: string,
    // public films: [],
    // public pilots: [],
    // public url: string,
    // public created: string,
    // public edited: string
  ) {}
}