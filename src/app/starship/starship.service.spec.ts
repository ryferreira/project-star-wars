import { TestBed } from '@angular/core/testing';

import { StarshipService } from './starship.service';

describe('StarshipServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: StarshipService = TestBed.get(StarshipService);
    expect(service).toBeTruthy();
  });
});
