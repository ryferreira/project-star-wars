 import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HttpClientModule } from '@angular/common/http'; 
import { MoviesComponent } from './movies/movies.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {MatTableModule} from '@angular/material';

import { MovieService } from './movies/movie.service';
import { MovieDetailsComponent } from './movie-details/movie-details.component';
import { RouterModule } from '@angular/router';
import { TitleComponent } from './title/title.component';
import { StarshipComponent } from './starship/starship.component';
import { StarshipService } from './starship/starship.service';

@NgModule({
  declarations: [
    AppComponent,
    MoviesComponent,
    MovieDetailsComponent,
    TitleComponent,
    StarshipComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule,
    MatTableModule,
    BrowserAnimationsModule
  ],
  providers: [
    MovieService,
    StarshipService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
