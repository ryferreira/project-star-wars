import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Movie } from '../movies/movies.component';
import { MovieService } from '../movies/movie.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.css']
})
export class MovieDetailsComponent implements OnInit {
  displayedColumns: string[] = ['episode_id', 'title', 'release_dates'];
  starshipsId: number[] = [];
  episode_id: number;
  movie: Movie;

  constructor(private activeRoute: ActivatedRoute, private movieService: MovieService) { }

  ngOnInit() {
    this.episode_id = this.activeRoute.snapshot.params['id'];
    
    
    this.movieService.getMovie(this.episode_id)
      .subscribe(movie => {
        this.movie = movie;

        this.getStarsihpsId();
        // console.log(">>>>>>>>>>>>> " + this.movie.starships[0]);
      });

    // console.log("STAR: " + this.starshipsId);

    // console.log("Starship: " + this.movie.starships[0]);
  }

  getStarsihpsId() {
    let link: string;
    let temp: string[];
    let length: number;
    // let aux: string;

    for (link of this.movie.starships) {
      temp = link.split('/');
      length = temp.length;
      this.starshipsId.push(Number(temp[length-2]));
    }

    this.movie.starshipsId = this.starshipsId;

    // console.log(this.starshipsId);

    
    // aux = this.movie.starships[0];
    
    // temp = aux.split('/');
    // length = temp.length;
    // temp = Number(this.movie.starships[0].split('/')[length-2]);
    // console.log("NUMBER: " + temp[length-2]);
    // debugger
  }



}
