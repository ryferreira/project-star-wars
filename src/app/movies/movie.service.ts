import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import { MOVIE_API } from './movie.api';

import { Movie } from './movies.component';

@Injectable({
  providedIn: 'root',
})
export class MovieService {
  movies: Movie[];

  constructor(private http: HttpClient) { }

  private extractData(res: Response) {
    let body = res;
    return body || {};
  }

  getMovies() {
    return this.http.get<Movie[]>(`${MOVIE_API}/films/`);
  }

  getMovie(id: number) {
    return this.http.get<Movie>(`${MOVIE_API}/films/${id}/`);
   //   .pipe(map(this.extractData));
  }
}
