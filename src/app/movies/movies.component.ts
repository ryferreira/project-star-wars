import { Component, OnInit } from '@angular/core';
import { MatTable} from '@angular/material/table'
import { MovieService } from './movie.service';
import { map } from 'rxjs/operators';
import { debug } from 'util';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.css']
})
export class MoviesComponent implements OnInit {
  displayedColumns: string[] = ['id', 'title', 'release_date'];
  // movie = [{
  //   episodeId: 4,
  //   name: "A New Hope",
  //   releaseDate: "1977-05-25"
  // }];
  dataSource: Movie[];
  // this.dataSource = this.movie;

  movie: Movie;

  // movies: Movie[];

  constructor(private movieService: MovieService) { }

  ngOnInit() {
    this.movieService.getMovies()
      .subscribe(movies => {
        // console.log(movies["results"]);
        // debugger 
        this.dataSource = movies['results'];
        this.sortData(this.dataSource);
        this.addId(this.dataSource);
        console.log(this.dataSource);
      });
  }

  sortData(data) {
    data.sort((a: Movie, b: Movie) => a.release_date > b.release_date ? 1 : -1)
  }

  addId(data) {
    for (let i in data) {
      data[i]["id"] = Number(i) + Number(1);
      // console.log(data[i]);
      // debugger
    }

  }
}

export class Movie {

  constructor(
    public title: string,
    public id: number,
    public release_date: string,
    public starships: string[],
    public starshipsId: number[],
    // public opening_crawl: string,
    // public director: string,
    // public producer: string,
    // public characters: [],
    // public vehicles: [],
    // public species: [],
    // public planets: [],
    // public created: string,
    // public edited: string,
    // public url: string
  ) {}
}